clear
clc

disp('loading data')
learn_data = dataset('file','YPMSD_Learn.txt','delimiter',',');
test_data = dataset('file','YPMSD_Test.txt','delimiter',',');

size(learn_data)

years = learn_data(:, 1);
%infos = horzcat(mat2dataset(ones(size(years))), );
infos=learn_data(:, 2:13);
%size(infos)
%size(years)
Y =double(years);
X =double(infos);
disp('creating model')
[Mdl,FitInfo] = fitrlinear(X,Y)

nbTest = size(test_data);
nbTest = nbTest(1,1);
disp(['testing ' num2str(nbTest) ' lines'])
nbBon = 0;
res = zeros(nbTest, 2);
diff = 0;
for c = 1:nbTest 
    if mod(c,10000)==0 
        disp(['line ' num2str(c) '=>' num2str(c/nbTest) '%'])
    end
    X =learn_data(c, 2:13);
    y =single(learn_data(c, 1));
    y_predict = single(predict(Mdl,double(X)));
    res(c,1)= y;
    res(c,2)=  y_predict;
    diff = diff + abs(y-y_predict);
    if round(y_predict)>= y-2 && round(y_predict)<= y+2
    %if round(y_predict)== y
        nbBon = nbBon +1;
    end
end

disp('fini')
disp(['nbBon :' num2str(nbBon) ' => ' num2str(nbBon/nbTest) '%'])
disp(['diff moyen ' num2str(diff/nbTest)])

%return 

resFile = fopen('res.12.2.txt','w');
fprintf(resFile,'%5d, %5d \n',res.');
fclose(resFile);

return


