import quandl, math
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from sklearn import preprocessing, cross_validation, svm
from sklearn.linear_model import LinearRegression


##### LIST OF FUNCTIONS #####

# PRINT THE NUMBER OF ROW AND COL OF 2D ARRAY
def rowcolNumber (data) :
    numrow = len(data)
    numcol = len(data[0])
    print(numrow)
    print(numcol)
    return

# RETURN A LIST OF THE YEARS SORTED
def yearsSort (data) :
    years = []
    for row in data:
        if int(row[0]) not in years:
            years.append(int(row[0]))
            years.sort()
    return(years)

# PRINT THE HISTOGRAMME OF THE DATA
def histogramme (data) :
    number = {}
    for row in data:
        if int(row[0]) in number:
            number[int(row[0])] = number[int(row[0])] + 1
        else:
            number[int(row[0])] = 1

    lists = sorted(number.items())
    x,y = zip(*lists)

    plt.plot(x,y)
    plt.show()
    return

def timbreAverage (data) :
    liste = []
    for i in range(0,12):
        liste.append(0)
        liste.append(9999)
        liste.append(-9999)


    nb = 0
    for row in data:
        nb += 1
        for i in range(0,12):
            liste[0+(i*3)] += row[i+1]
            if liste[1+(i*3)] > row[i+1]:
                liste[1+(i*3)] = row[i+1]
            if liste[2+(i*3)] < row[i+1]:
                liste[2+(i*3)] = row[i+1]

    for i in range(0,12):
        liste[0+(i*3)] /= nb

    x = [1,2,3,4,5,6,7,8,9,10,11,12]

    y = []
    for i in range(0,12):
        y.append(liste[0+(i*3)])

    e = []
    for i in range(0,12):
        e.append( (liste[2+(i*3)]-liste[1+(i*3)])/2 )


    for i in range(0,12):
        plt.errorbar(x[i], y[i], xerr=0.2, yerr=e[i])

    plt.show()

    return

def calculateError(data):
    i = 0
    true = 0
    one = 0
    two = 0
    three = 0
    four = 0
    five = 0
    false = 0
    for row in data:
        i += 1
        year = row[0]
        predict = math.floor(row[1])
        if (year == predict):
            true += 1
        elif(year == ((predict+1) or (predict-1))):
            one += 1
        elif(year == ((predict+2) or (predict-2))):
            two += 1
        elif(year == ((predict+3) or (predict-3))):
            three += 1
        elif(year == ((predict+4) or (predict-4))):
            four += 1
        elif(year == ((predict+5) or (predict-5))):
            five += 1
        else:
            false += 1

    labels = 'True', '+/- 1', '+/- 2', '+/- 3', '+/- 4', '+/- 5', 'False'
    sizes = [true, one, two, three, four, five, false]
    colors = ['green', 'yellowgreen', 'y', 'yellow','orange','orangered', 'red']
    explode = (0.1, 0, 0, 0, 0, 0, 0.1)

    plt.pie(sizes, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
    plt.axis=('equal')
    plt.show()

    return

def calculateErrorPerYear (data):
    number = {}
    true = {}
    one = {}
    two = {}
    three = {}
    four = {}
    five = {}
    false = {}
    for row in data:
        year = row[0]
        predict = math.floor(row[1])

        if year in number:
            number[year] += 1
        else:
            number[year] = 1

        if (year == predict):
            if year in true:
                true[year] += 1
            else:
                true[year] = 1
        elif(year == ((predict+1) or (predict-1))):
            if year in one:
                one[year] += 1
            else:
                one[year] = 1
        elif(year == ((predict+2) or (predict-2))):
            if year in two:
                two[year] += 1
            else:
                two[year] = 1
        elif(year == ((predict+3) or (predict-3))):
            if year in three:
                three[year] += 1
            else:
                three[year] = 1
        elif(year == ((predict+4) or (predict-4))):
            if year in four:
                four[year] += 1
            else:
                four[year] = 1
        elif(year == ((predict+5) or (predict-5))):
            if year in five:
                five[year] += 1
            else:
                five[year] = 1
        else:
            if year in false:
                false[year] += 1
            else:
                false[year] = 1



    numberList = sorted(number.items())
    x1,y1 = zip(*numberList)
    y1max = max(y1)
    x1posmax = y1.index(y1max)
    x1max = x1[x1posmax]

    trueList = sorted(true.items())
    x2,y2 = zip(*trueList)
    y2max = max(y2)
    x2posmax = y2.index(y2max)
    x2max = x2[x2posmax]

    oneList = sorted(one.items())
    x3,y3 = zip(*oneList)
    y3max = max(y3)
    x3posmax = y3.index(y3max)
    x3max = x3[x3posmax]

    twoList = sorted(two.items())
    x4,y4 = zip(*twoList)
    y4max = max(y4)
    x4posmax = y4.index(y4max)
    x4max = x4[x4posmax]

    threeList = sorted(three.items())
    x5,y5 = zip(*threeList)
    y5max = max(y5)
    x5posmax = y5.index(y5max)
    x5max = x5[x5posmax]

    fourList = sorted(four.items())
    x6,y6 = zip(*fourList)
    y6max = max(y6)
    x6posmax = y6.index(y6max)
    x6max = x6[x6posmax]

    fiveList = sorted(five.items())
    x7,y7 = zip(*fiveList)
    y7max = max(y7)
    x7posmax = y7.index(y7max)
    x7max = x7[x7posmax]

    falseList = sorted(false.items())
    x8,y8 = zip(*falseList)
    y8max = max(y8)
    x8posmax = y8.index(y8max)
    x8max = x8[x8posmax]

    plt.xlabel("Years")
    plt.ylabel("Number of song")
    plt.plot(x1,y1, label="Total")
    plt.plot(x2,y2, label="True")
    plt.plot(x3,y3, label="+/- 1")
    plt.plot(x4,y4, label="+/- 2")
    plt.plot(x5,y5, label="+/- 3")
    plt.plot(x6,y6, label="+/- 4")
    plt.plot(x7,y7, label="+/- 5")
    plt.plot(x8,y8, label="False")

    #plt.axvline(x=x1max,linestyle='--')
    #plt.axvline(x=x2max,linestyle='--')
    #plt.axvline(x=x3max,linestyle='--')
    #plt.axvline(x=x4max,linestyle='--')
    #plt.axvline(x=x5max,linestyle='--')
    #plt.axvline(x=x6max,linestyle='--')
    #plt.axvline(x=x7max,linestyle='--')
    #plt.axvline(x=x8max,linestyle='--')

    plt.legend()
    plt.show()
    return

def yearPredicted (data):
    x1 = []
    for x in range(1980, 2011):
        x1.append(x)

    result = {}
    predicted={}
    number={}
    for row in data:
        year = row[0]
        predict = math.floor(row[1])

        if year in number:
            number[year] += 1
        else:
            number[year] = 1

        if year in predicted:
            predicted[year] += predict
        else:
            predicted[year] = predict

        for y in predicted.keys():
            result[y] = predicted[y] / number[y]

    r = sorted(result.items())
    x,y = zip(*r)

    plt.plot(x,y, label="predicted");
    plt.plot(x1,x1, label="droite parfaite")
    plt.legend();
    plt.show();
    return

##### END OF THE FUNCTIONS #####

print("######################")
print("HELLO TO THE IA SONGYEAR PROGRAM")
print("For calculate data from the songyear file, type 1")
print("For calculate data from the res, type 2")
print("######################")

choice = 0
while (choice not in (1, 2)):
    print("your choice ? [1, 2]: ")
    choice = input()

    if choice == 1 :
        data = []

        # OPEN FILE OF YEARPREDICTION AND STORE IT IN data[]
        with open("./YearPredictionMSD.txt",'r') as f:
            for line in f:
                number_strings = line.split(',')
                numbers = [float(n) for n in number_strings]
                data.append(numbers)

        functionChoice = 99
        while (functionChoice not in (0, 1)):
            print("What function do you want to use ?")
            print("-- to quit the program, print 0")
            print("-- to go back to the main menu, print 1")
            print("-- to print the number of row & col from data, print 2")
            print("-- to print the years from the file, sorted, print 3")
            print("-- to print the histogram, print 4")
            print("-- to print the timber average, print 5")
            functionChoice = input()

            if (functionChoice == 1):
                choice = 0
            if (functionChoice == 2):
                rowcolNumber(data);
            if (functionChoice == 3):
                yearsSort(data);
            if (functionChoice == 4):
                histogramme(data);
            if (functionChoice == 5):
                timbreAverage(data);


    if (choice == 2):
        print("Choose the file you want to calculate error on")
        files = [f for f in os.listdir('./res') if os.path.isfile(os.path.join('./res', f))]
        for f in files:
            if f.endswith(".txt"):
                print(f)

        print("Your choice: ")
        fileChoice = raw_input()
        data = []

        # OPEN FILE OF YEARPREDICTION AND STORE IT IN data[]
        with open("./res/"+fileChoice,'r') as f:
            for line in f:
                number_strings = line.split(',')
                numbers = [float(n) for n in number_strings]
                data.append(numbers)

        functionChoice = 99
        while (functionChoice not in (0, 1)):
            print("What function do you want to use ?")
            print("-- to quit the program, print 0")
            print("-- to go back to the main menu, print 1")
            print("-- to calculate the error on this file, print 2")
            print("-- to calculate the error by year, print 3")
            print("-- to print the year - predicted graph, print 4")
            functionChoice = input()

            if (functionChoice == 1):
                choice = 0
            if (functionChoice == 2):
                calculateError(data);
            if (functionChoice == 3):
                calculateErrorPerYear(data);
            if (functionChoice == 4):
                yearPredicted(data);
